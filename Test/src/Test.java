import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by a.horoshailov on 27.02.2018.
 */
public class Test {
    public static void main(String[] args) {
        String filePath = System.getProperty("user.dir") + "\\";
        String fileName = "test.txt";
        int countNumbers = 20;
        Integer[] countNumbersInFile = new Integer[countNumbers + 1];

        for (int i = 0; i < countNumbersInFile.length; i++){
            countNumbersInFile[i] = i;
        }

        Test factorial = new Test();
        factorial.calculateFactorial(8);

        String shuffleArrayText = shuffleArray(countNumbersInFile);

        Test newFile = new Test();
        newFile.CreateAndFillFile(filePath, fileName, shuffleArrayText);

        sortingAsc(getFileData(filePath, fileName));
        sortingDesc(getFileData(filePath, fileName));
    }

    //создаётся и заполняется файл
    public void CreateAndFillFile(String filePath, String fileName, String text){
        try{
            FileWriter writer = new FileWriter(filePath + fileName, false);
            writer.write(text);
            writer.flush();
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    //перемешивается массив данных. Возвращается строка
    public static String shuffleArray(Integer[] array ){
        String shuffleString = "";

        List<Integer> lst = Arrays.asList(array);
        Collections.shuffle(lst);
        array = lst.toArray(array);

        for (int i = 0; i < array.length; i++) {
            if (i == array.length - 1){
                shuffleString += array[i];
            }else {
                shuffleString += array[i] + ",";
            }
        }
        return shuffleString;
    }

    //Данные из файла записываются в строку, разбиваются на значения массива и конвертируются в int. Возвращается целочисленный массив
    public static int[] getFileData(String filePath, String fileName){
        try{
            FileReader reader = new FileReader(filePath + fileName);
            int c;
            String s = "";
            while((c = reader.read()) != -1){
                s += String.valueOf((char) c);
            }
            String[] stringFileData = s.split(",");
            int[] intFileData = new int[stringFileData.length];
            for (int i = 0; i < stringFileData.length; i++){
                intFileData[i]  = Integer.parseInt(stringFileData[i]);
            }

            return intFileData;
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
       return null;
    }

    //Сортировка методом пузырька по возрастанию
    public static void sortingAsc(int[] array){
        System.out.println("Sorting Asc(original): " + Arrays.toString(array));
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int t = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = t;
                }
            }
        }
        System.out.println("Sorting Asc(sort): " + Arrays.toString(array));
    }

    //Сортировка методом пузырька по убыванию
    public static void sortingDesc(int[] array){
        System.out.println("Sorting Desc(original): " + Arrays.toString(array));
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] < array[j + 1]) {
                    int t = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = t;
                }
            }
        }
        System.out.println("Sorting Desc(sort): " + Arrays.toString(array));
    }

    //Вычисление факториала
    public void calculateFactorial(int number){
        int n = number;
        int factorial = 1;
        while (n > 0){
            factorial *= n;
            n--;
        }
        System.out.println("Factorial " + number + " = " + factorial);
    }
}
